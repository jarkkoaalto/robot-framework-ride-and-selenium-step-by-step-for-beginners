# Robot-Framework-RIDE-And-Selenium-Step by-Step-for-Beginners

In this course we will learn Robot Framework from scratch.
Here we will use Robot Framework to test Web Browser applications using Selenium library and RIDE (Robot Framework IDE)

# Command Line tips

1 How to run tests command prompt
2 How to run selected tests
3 How to run tests with TAGS
4 How to run all Tests in a Suite
5 How to send RESULTS to a specific folder

Useful TIPS

Step 1: Open cmd and goto the location of project

Step 2: Run single test
	robot -t TestName Suite

Step 3: Run multiple tests
	robot -t Test01 -t Test05 SuiteFile

Step 4: Run Tests with TAGS
	robot -i Sanity Test05 SuiteFile.txt/.robot
	or
	robot --include Sanity Test05 SuiteFile.txt/.robot

	Multible:
	robot -i Sanity -i Smoke TestSuite.txt

	ADD Regular Expression
	robot -i S* TestSuite.txt // Run all test what start S(uite) S(moke) e.g.

Step 5: Run all test

	robot TestSuite.txt

Step 6: Send results to a folder
	robot -t Test05 -d Result TestSuite.txt // Create folder named Result and store report, log and output files folder

Useful Tips

1.) You can use re commands 
	Exlude: 
	robot --exclude S* Testsuite.txt or robot -e S* Testsuite.txt

2.) How to make tags critical / non-critical
	robot -i S* --critical Smoke TestSuite.txt

	robot -i S* --noncritical (or -n) Smoke TestSuite.txt

3.) Robot framework Command Line Options
https://github.com/robotframework/robotframework/blob/master/doc/userguide/src/Appendices/CommandLineOptions.rst
 
 # How to run tests from JENKINS

 1.) How to create job in jenkins
 2.) How to add robot framework plugin in JENKINS
 3.) How to configure robot framework test in JENKINS
 4.) Show html report in JENKINS
 5.) Run and Validate

 Step 1: Download jenkins
    https://jenkins.io/download/
    take 
    Generic Java Package(.war)

Step 2: Go to the Download folder or where you removed jenkins.war and type add --http=9191 if you want change port
    java -jar jenkins.war --httpPort=9191

Step 3: Open browser localhost:9191 create account and ...

Step 4: Add robot framework plugin
       manage plugin ...

Step 5: Create job to run robot framework test
    Use Build => Execute Windows batch command
    running your testcases:

    C:
    cd C:\Users\User\GITREPO\robot-framework-ride-and-selenium-step-by-step-for-beginners\section04
    robot -d Results -t Test04 LoginTest.robot

Go to  the Post-build Action - section and select drop-down menu :
    Publish Robot Framework test results

Promt:
Directory of Robot output location path
C:\Users\User\GITREPO\robot-framework-ride-and-selenium-step-by-step-for-beginners\section04\Results

Step 6: Run: Build Now

and see result 


IF YOU CANNOT SEE REPORT.HTML and OTHER REPORT
You have go Script Console run and validate script:
System.setProperty("hudson.model.DirectoryBrowserSupport.CSP","")

# Tips and Tricks

1.) Install latest version
    pip install robotframework

2.) Install a specifig version
    pip install robotframework==2.9.0

3.) Do a freash install ignoring cached files
    pip install --no-cache-dir robotframework

4.) Version check and find help
    robot --version
    robot --help

5.) Upgrade robot
    pip install --upgrade robotframework

6.) Uninstall robot
    pip uninstall robotframework

7.) Check robot reporting version and find help
    rebot --version
    rebot --help
