*** Settings ***
Library           Selenium2Library

*** Variables ***
${url}            https://opensource-demo.orangehrmlive.com/
@{CREDENTIALS}    Admin    admin123
&{LOGIN}          \    UserName = Admin    Password = admin123

*** Test Cases ***
Test02
    Selenium2Library.Open Browser    ${url}    chrome
    Input Text    id=txtUsername    @{CREDENTIALS}[0]
    Input Password    id=txtPassword    @{CREDENTIALS}[1]
    Click Button    id=btnLogin
    Close Browser
    Log To Console    %{username} ran this test on %{os}

Test03
    [Tags]    Test03
    Open Browser    ${url}    chrome
    Login
    Close All Browsers

Test04
    [Tags]    Test04
    Log To Console    Test04 Started
    Open Browser    https://google.fi    chrome
    Close All Browsers
    Log To Console    Test04 Complete
    Set Tags    SmokeTest

Test05
    [Tags]    Sanity
    Log To Console    Test 05 Empty Test

*** Keywords ***
Login
    Input Text    id=txtUsername    Admin
    Input Password    id=txtPassword    Admin123
    Click Button    id=btnLogin
