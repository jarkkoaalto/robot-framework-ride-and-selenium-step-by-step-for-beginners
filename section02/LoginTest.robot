*** Settings ***
Suite Setup       Go To Home Page
Suite Teardown    Close All Browsers
Library           Selenium2Library

*** Variables ***
${url}            https://opensource-demo.orangehrmlive.com/
@{CREDENTIALS}    Admin    admin123
&{LOGIN}          \    UserName = Admin    Password = admin123

*** Test Cases ***
Test03
    [Tags]    Test03
    Login

*** Keywords ***
Login
    Input Text    id=txtUsername    Admin
    Input Password    id=txtPassword    Admin123
    Click Button    id=btnLogin

Go To Home Page
    Open Browser    ${url}    chrome
